# Auditoria SDK

Modulo de registro de logs en el servicio de auditoria.

## Configuration env

```js
#Audit properties to add it.
AUDIT_SERVICE_ENABLED=true
BASE_PATH_AUDIT_SERVICE=http://192.168.0.211:8095
AUDIT_SERVICE_HEADER=Audit-Trace-Id
AUDIT_SERVICE_USERNAME=midas
AUDIT_SERVICE_PASSWORD=Bigfun22!
AUDIT_SERVICE_REQ_USER=payload.email
AUDIT_SERVICE_LOGGER_LEVEL=debug
```

1. AUDIT_SERVICE_ENABLED => Servicio activo o inactivo. (true | false)
2. BASE_PATH_AUDIT_SERVICE => Url de servicio de auditoria
3. AUDIT_SERVICE_HEADER => Nombre del header que representa la Traza
4. AUDIT_SERVICE_USERNAME => Username cliente de auditoria.
5. AUDIT_SERVICE_PASSWORD => Password cliente de auditoria.
6. AUDIT_SERVICE_REQ_USER => Nombre de la propiedad a buscar en ´´´req´´´ al usuario
7. AUDIT_SERVICE_LOGGER_LEVEL => Nivel de log, default "debug"

## Middleware initial

En app.js que es el entrypoint de nuestro aplicación en node, necesitamos importar la función signin para registrarse al servicio de auditoría.
Este paso es realmente necesario para poder escribir en los logs del servicio de auditoría.

### Express

```js
const {handlerMiddleware} = require('midas-audit-sdk');
 // Enabled Authentication Module
 app.use('/api/', passport.authenticate('jwt', {session: false}));
 // Middleware q obtiene req.user, req.header[trace-header]
 app.use(handlerMiddleware);
 ```

### Nestjs

```js
[const {handlerMiddleware} = require('midas-audit-sdk');
 // Enabled Authentication Module
 app.use('/api/', passport.authenticate('jwt', {session: false}));
 // Middleware q obtiene req.user, req.header[trace-header]
 app.use(handlerMiddleware);](import { Module, NestModule, MiddlewareConsumer } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CatsModule } from './cats/cats.module';
import * as dotenv from 'dotenv';
import { handlerMiddleware } from 'midas-audit-sdk';
import { MongooseModule } from '@nestjs/mongoose';
import { AuthModule } from './authentication/auth.module';

@Module({
 imports: [CatsModule, MongooseModule.forRoot('mongodb://localhost/nest'), AuthModule],
 controllers: [AppController],
 providers: [AppService],
})
export class AppModule implements NestModule {
 configure(consumer: MiddlewareConsumer) {
   // initialize dotenv config - Not necessary in version 1.2.3
   dotenv.config();
   consumer
     // apply middleware to get user and traceID
     .apply(handlerMiddleware)
     .forRoutes('cats');
 }
}
)
 ```

## @Checkpoint

La segunda forma de utilizar el SDK es mediante la anotación @Chekpoint.

```js
@Checkpoint({ action: 'WRITE', includeParams: true, behavior: 'AROUND' })
public async create(createCatDto: CreateCatDto): Promise<Cat> {
   const createdCat = new this.catModel(createCatDto);
   return createdCat.save();
}
```

La anotación funciona a nivel de método.
Cada vez que se invoque el método anotado, previo a su ejecución se invocará al servicio de auditoría escribiendo la información configurada según los parámetros de la anotación:

<action:string>: Nombre de la acción

<includeParams:boolean: optional default true>: Si es true, todos los parámetros del método anotado se incluirán en el registro como el payload de la acción

<behavior:BEFORE | AFTER | AROUND>: Indica la información que debe escribir el servicio:
BEFORE: Escribe únicamente los parámetros del método en el campo payloadIn
AFTER: Escribe únicamente el valor de retorno del método en el campo payloadOut
AROUND: Escribe los parámetros del método en el campo payloadIn y el valor de retorno en el campo payloadOut

## Middelware error/success

A nivel de middleware de express, podemos loguear después de que las rutas están definidas para loguearse automáticamente los errores o success de cada endpoint.

### Express

```js
const {handlerError, handlerSuccess} = require('midas-audit-sdk');
app.use(handlerSuccess);
app.use(handlerError);

```

Recibe error, request, response de: 
Action: request.url
PayloadIn: request.params // request.swagger.params
PayloadOut: err

## Wrapper

La anotación funciona a nivel de función. Requiere que las funciones se exporten individualmente en cada archivo js.
Cada vez que se invoque el método anotado, previo a su ejecución se invocará al servicio de auditoría escribiendo la información configurada según los parámetros de la anotación:

<action:string>: Nombre de la función

<payloadIn - parámetros de la funcion>: Todos los parámetros del método anotado se incluirán en el registro como el payload de la acción

<behavior: AROUND>: Indica la información que debe escribir el servicio:

AROUND: Escribe los parámetros del método en el campo payloadIn y el valor de retorno en el campo payloadOut

```js
import {log} from 'midas-audit-sdk';

const createNewUser = (payload) => new Promise((resolve, reject) => {
 User.register(payload, payload.password, (err, user) => {
   if (err) {
     reject(err);
   }
   return resolve(user);
 });
});

module.exports = {
 createNewUser: log(createNewUser)
};
```

## Checkpoint

Es una función de node, que se agrega a cualquier clase/archivo js mediante la siguiente anotación.

```js
const {hit} = require('midas-audit-sdk');
```

El componente contiene una única función sobrecargado con más o menos parámetros según la necesidad:

```js
hit = ({ action, payloadIn, payloadOut, traceId })
```

Recibe objeto json que tiene las propiedades: action, payloadIn, payloadOut, traceId.
El payload (in/out) puede ser cualquier objeto json. Este método va a generar un traceId nuevo siempre y cuando el traceId sea null o no haya uno previamente en la sesión. 
Ejemplo de uso:

```js
const {hit} = require('midas-audit-sdk');

public void method() {
 hit({action: “WRITE”,payloadIn:{}, traceId: “123456” });
}
```
